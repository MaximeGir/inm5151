INM5151
=======

Membres
-------

  * Eric Fournier (FOUE01058109) (chef d'équipe)
  * Guillaume Lahaie (LAHG04077707)
  * Marco Gagliano (GAGM24068009)
  * Maxime Girard (GIRM30058500)

Projet et Description (Approuvé en classe ce matin)
--------------------------------------------------

Projet : Système note de cours

Description : Logiciel qui génère des présentations (acétates) à partir de documents rédigés dans un format très simple.

------------------------------------------------------------------------------------------------------------------------

Comment installer le prototype

il faut avoir node et npm installés sur votre ordi

* $ cd application-express
* $ npm install
* $ node app.js

ou l'equivalent sur windows
